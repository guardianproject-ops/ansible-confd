# ansible-confd

An Ansible role that downloads and installs [confd](https://github.com/kelseyhightower/confd) on an amd64 debian system.

# Requirements

* Debian

# Role Variables

### Required Variables

none

### Default Variables

See [defaults/main.yml](defaults/main.yml)

# Dependencies

none

# Example Playbook

    - hosts: servers
      roles:
         - { role: ansible-confd }

# Testing

```
molecule test --all
```

# License

[GNU Affero General Public License (AGPL) v3+](https://www.gnu.org/licenses/agpl-3.0.en.html)

# Author Information

* Abel Luck <abel@guardianproject.info> of [Guardian Project](https://guardianproject.info)

