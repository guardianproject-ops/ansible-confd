import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_directories(host):
    dirs = [
            "/etc/confd",
            "/etc/confd/conf.d",
            "/etc/confd/templates"
    ]
    for dir in dirs:
        d = host.file(dir)
        assert d.is_directory
        assert d.exists


def test_files(host):
    files = [
            "/etc/confd/confd.toml",
    ]
    for file in files:
        f = host.file(file)
        assert f.exists
        assert f.is_file


def test_service(host):
    pass


def test_socket(host):
    sockets = [
    ]
    for socket in sockets:
        s = host.socket(socket)
        assert s.is_listening
